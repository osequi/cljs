# REPLs

## Calva

- https://calva.io/getting-started/

### Commands

- `Ctrl+Alt+C Enter` -> Load file
- `Ctrl Enter` -> Eval anything (forms)
- `Alt Enter` -> Eval top level

### Notes

- Anything printed to stdout (`println`) is not shown inline
